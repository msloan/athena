/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_IOGENEVENT_H
#define ATLASHEPMC_IOGENEVENT_H
#include "HepMC/IO_GenEvent.h"
#endif
